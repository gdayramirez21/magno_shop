export const getPages = (array, itemsPerPage) => {
    return  array.length / itemsPerPage
}

export const setResultsByPage = (page, all, pages, itemsByPage) => {
    let array = []
    if (page === 1)
        for (let i = 0; i < (all.length < itemsByPage ? all.length : itemsByPage); i++)
            array.push(all[i])
    else if (page !== 1 && page !== pages)
        for (let i = (page - 1) * itemsByPage; i < (page) * itemsByPage; i++)
            array.push(all[i])
    else
        for (let i = (page - 1) * itemsByPage; i < all.length; i++)
            array.push(all[i])
    return array
}

export const makeSort = (items, key, by) => {
    switch (by) {
        case 'asc': return items.sort(function (a, b) {
            if (a[key] > b[key]) {
                return 1;
            }
            if (a[key] < b[key]) {
                return -1;
            }
            return 0;
        });
        case 'desc':
            return items.sort(function (a, b) {
                if (a[key] < b[key]) {
                    return 1;
                }
                if (a[key] > b[key]) {
                    return -1;
                }
                return 0;
            });
        default: return items
    }
}


export const verifyitems = (array, items) => array.map((e) => {
    if (Array.isArray(e.Price) && e.Price.length > 0){
            let priceOffer = e.Price.find(price => price.priceDescription === 'I') 
        if (priceOffer) {
            e.lastPrice = parseFloat(priceOffer.priceValue)
        }
        else {
            let priceList = e.Price.find(price => price.priceDescription === 'L')
            e.lastPrice = priceList ? parseFloat(priceList.priceValue) : 0
        } 
    }
    else {
        e.lastPrice =  0
    }
    let found = items.find( element => element.id === e.uniqueID)
    if (found) e.isFavorite = true
    return e;
})