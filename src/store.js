import {createStore, applyMiddleware, compose} from 'redux'
/**
 * Importando Reducer Principal
 */
import itemsReducer from './reducers/itemsReducer'
/**
 * Agregamos Redux-Saga 
 */
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
/**
 * Creamos un middleware
 */
const sagaMiddleware = createSagaMiddleware();
/**
 * Creamos un Store
 */
const store = createStore(itemsReducer,compose(applyMiddleware(sagaMiddleware),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));
/**
 * Ejecutamos nuestra Saga
 */
sagaMiddleware.run(rootSaga);

export default store;