import {
    MOSTRAR_CARRITO,
    AGREGAR_ITEM,
    BORRAR_ITEM,
    AGREGAR_RESULTS,
    GET_SEARCH,
    SET_TOTAL_ITEMS_PER_PAGE,
    CHANGE_PAGE,
    AGREGAR_PAGE_RESULTS,
    SET_TOTAL_PAGES,
    SORT_BY_NAME,
    SORT_BY_PRICE
} from '../actions/types';

import { getPages, setResultsByPage, makeSort } from '../utils'

const storageState = localStorage.getItem('state') ? JSON.parse(localStorage.getItem('state')) : {
    items: [],
    results: [],
    activePage: 1,
    totalPages: 1,
    itemsPerPage: 12,
    resultsPage: [],
    sortByNameResult : '',
    sortByPriceResult : ''
}



export default function (state = storageState, action) {
    let found = undefined
    let resultsPage = undefined
    let totalPages = undefined
    switch (action.type) {
        case SORT_BY_PRICE : {
            let sortResults = makeSort(state.results,'lastPrice',action.payload)
            resultsPage = setResultsByPage(state.activePage, sortResults, state.totalPages, state.itemsPerPage)
            return {
                ...state,
                sortByPriceResult : action.payload,
                sortByNameResult : '',
                resultsPage,
            }
        }
        case SORT_BY_NAME : {
            let sortResults = makeSort(state.results,'name',action.payload)
            resultsPage = setResultsByPage(state.activePage, sortResults, state.totalPages, state.itemsPerPage)
            return {
                ...state,
                sortByNameResult : action.payload,
                sortByPriceResult : '',
                resultsPage,

            }
        }
        case CHANGE_PAGE:
            resultsPage = setResultsByPage(action.payload, state.results, state.totalPages, state.itemsPerPage)
            return {
                ...state,
                resultsPage,
                activePage: action.payload
            }
        case SET_TOTAL_ITEMS_PER_PAGE:
            totalPages = getPages(state.results, action.payload)
            resultsPage = setResultsByPage(1, state.results, totalPages, action.payload)
            return {
                ...state,
                itemsPerPage: action.payload,
                totalPages,
                resultsPage,
                activePage: 1
            }
        case MOSTRAR_CARRITO:
            return {
                ...state
            }
        case AGREGAR_ITEM:
            found = state.results.find(element => element.uniqueID === action.payload.id)
            if (found) {
                found.isFavorite = true
            }
            return {
                ...state,
                items: [...state.items, action.payload]
            }
        case BORRAR_ITEM:
            found = state.results.find(element => element.uniqueID === action.payload)
            if (found) {
                found.isFavorite = false
            }
            return {
                ...state,
                items: state.items.filter(favorite => favorite.id !== action.payload)
            }
        case AGREGAR_RESULTS:
            return {
                ...state,
                results: action.payload
            }
        case AGREGAR_PAGE_RESULTS:
            return {
                ...state,
                resultsPage: action.payload
            }
        case GET_SEARCH:
            return {
                ...state,
                search: action.payload.search
            }
        case SET_TOTAL_PAGES:
            return {
                ...state,
                totalPages: action.payload
            }
        default: return state;
    }
}