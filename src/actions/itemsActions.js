import {
    MOSTRAR_CARRITO,
    AGREGAR_ITEM,
    BORRAR_ITEM,
    MOSTRAR_RESULTS,
    AGREGAR_RESULTS,
    SET_SEARCH,
    GET_SEARCH,
    SET_TOTAL_ITEMS_PER_PAGE,
    CHANGE_PAGE,
    AGREGAR_PAGE_RESULTS,
    SET_TOTAL_PAGES,
    SORT_BY_NAME,
    SORT_BY_PRICE
} from './types';

export const changePage = (page)=>{
    return {
        type : CHANGE_PAGE,
        payload : page
    };
};

export const sortByName = (rule)=>{
    return{
        type: SORT_BY_NAME,
        payload : rule
    };
};


export const sortByPrice = (rule)=>{
    return{
        type: SORT_BY_PRICE,
        payload : rule
    };
};


export const setItemsPerPage = (value) =>{
    return {
        type : SET_TOTAL_ITEMS_PER_PAGE,
        payload : value
    };
};

export const obtenerCarrito= ()=>{
    return {
        type : MOSTRAR_CARRITO
    };
};

export const agregarItem = (favorito)=>{
    return {
        type : AGREGAR_ITEM,
        payload : favorito
    };
};

export const borrarItem = (id)=>{
    return {
        type : BORRAR_ITEM,
        payload : id
    };
};

export const obtenerResults= ()=>{
    return {
        type : MOSTRAR_RESULTS
    };
};

export const nuevosResults = (results)=>{
    return {
        type : AGREGAR_RESULTS,
        payload : results
    };
};

export const resultsPage = (results) => {
    return {
        type : AGREGAR_PAGE_RESULTS,
        payload : results
    };
};

export const setSearch = (value)=>{
    return {
        type : SET_SEARCH,
        payload : value
    };
};

export const setTotalPages = (value)=>{
    return {
        type : SET_TOTAL_PAGES,
        payload : value
    };
};

export const getSearch= (value)=>{
    return {
        type : GET_SEARCH,
        payload : value
    };
};

