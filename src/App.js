import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './components/Home'
import items from './components/Carrito'
import { Provider } from 'react-redux'
import store from './store'
import { MuiThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Theme from './theme';
import Styles from './styles'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <MuiThemeProvider theme={Theme}>
            <Route exact path="/" component={Home} />
            <Route path="/items" component={items} />
          </MuiThemeProvider>
        </Router>
      </Provider>
    );
  }
}

export default withStyles(Styles)(App);