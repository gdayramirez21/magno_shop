import { put, takeLatest, all } from 'redux-saga/effects';
import { TRENDING } from '../globals'
import { getPages, setResultsByPage,makeSort,verifyitems } from '../utils'

function* getSearchResults(params) {
    let { activePage, itemsPerPage,sortByNameResult,sortByPriceResult,items} = params.payload
    const json = yield fetch(TRENDING, { headers: { 'Content-Type': 'application/json' } }).then(data => data.json());

    let checkedItems = verifyitems(json.CatalogEntryView,items)
    let sortResults = makeSort(checkedItems,'lastPrice',sortByPriceResult)
    sortResults = makeSort(sortResults,'name',sortByNameResult)
    const pages = getPages(sortResults, itemsPerPage)
    const resultsPage = setResultsByPage(activePage, sortResults, pages, itemsPerPage)
    
    yield put({ type: "AGREGAR_RESULTS", payload: checkedItems });
    yield put({ type: "AGREGAR_PAGE_RESULTS", payload: resultsPage });
    yield put({ type: "SET_TOTAL_PAGES", payload: pages });
};


function* actionWatcher() {
    yield takeLatest('GET_SEARCH', getSearchResults)
}

export default function* rootSaga() {
    yield all([
        actionWatcher(),
    ]);
}