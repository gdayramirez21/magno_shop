import React from 'react';
import { Pagination } from 'semantic-ui-react'

import { connect } from 'react-redux'
import { changePage } from '../actions/itemsActions'

const InputAdornments = (props) => {
    const { totalPages , activePage} = props
    return (
        <Pagination
            activePage = {activePage}
            ellipsisItem={null}
            totalPages={totalPages}
            onPageChange = {(e, { activePage }) =>props.changePage(activePage)}
        />
    );
}

const mapStateToProps = state => ({
    totalPages: state.totalPages,
    activePage : state.activePage
})


export default connect(mapStateToProps, { changePage })( InputAdornments );




