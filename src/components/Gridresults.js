import React, { Component } from 'react'
import { connect } from 'react-redux'
import { obtenerCarrito, agregarItem, borrarItem } from '../actions/itemsActions'
import { withStyles } from '@material-ui/core/styles';
import Styles from '../styles'
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import { CardActionArea, CardContent, Typography } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import Pagination from './Pagination'


const Actions = props => {
    let { element, add, remove } = props
    if (element.isFavorite) {
        return (
            <Button size="small" color="primary" onClick={() => remove(element.uniqueID)}>
                Quitar de mi carrito
            </Button>
        )
    }
    else {
        return (
            <Button size="small" color="primary" onClick={() => add({
                id: element.uniqueID,
                url: 'https://qa.commerceonthecloud.com' + element.thumbnail,
                title: element.name ? element.name : 'without title',
                price: Array.isArray(element.Price) && element.Price.length > 0 ? element.Price[0].priceValue : ''
            })}>
                Agregar a mi carrito
        </Button>
        )
    }
}

class Home extends Component {

    addFavorite = favorite => this.props.agregarItem(favorite)

    removeFavorite = id => this.props.borrarItem(id)

    render() {
        let { classes } = this.props
        let resultsPage = this.props.resultsPage ? this.props.resultsPage : []
        return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Pagination></Pagination>
                    </Grid>
                    {
                        resultsPage.map((e) => {
                            if (e) {
                                return (
                                    <Grid item lg={3} md={3} sm={6}>
                                        <Card className={classes.card}>
                                            <CardActionArea>
                                                <CardMedia
                                                    component="img"
                                                    image={'https://qa.commerceonthecloud.com' + e.thumbnail} alt={e.uniqueID}
                                                />
                                                <CardContent>
                                                    <Typography gutterBottom variant="body2" >
                                                        {e.name}
                                                    </Typography>
                                                    <Typography gutterBottom variant="body2" >
                                                        {e.lastPrice ? `$ ${e.lastPrice}` : 'Precio No Disponible'}
                                                    </Typography>
                                                </CardContent>
                                            </CardActionArea>
                                            <CardActions>
                                                <Actions element={e} add={this.addFavorite} remove={this.removeFavorite}></Actions>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                )
                            }
                        })
                    }
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    results: state.results,
    items: state.items,
    resultsPage: state.resultsPage
})

export default connect(mapStateToProps, { obtenerCarrito, agregarItem, borrarItem })(withStyles(Styles)(Home));

