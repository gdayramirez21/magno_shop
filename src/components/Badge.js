import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import { withStyles } from '@material-ui/core/styles';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { connect } from 'react-redux'

const StyledBadge = withStyles(theme => ({
    badge: {
        top: '50%',
        right: -3,
        border: `2px solid ${
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
            }`,
    },
}))(Badge);

const CustomizedBadges = props => {
    let { items } = props
    return (
        <IconButton aria-label="Cart">
            <StyledBadge badgeContent={items.length} color="primary">
                <ShoppingCartIcon />
            </StyledBadge>
        </IconButton>
    );
}

const mapStateToProps = state => ({
    items: state.items
})

export default connect(mapStateToProps, null)(CustomizedBadges);
