import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux'
import { sortByName,setItemsPerPage,sortByPrice } from '../actions/itemsActions'

const ranges = [
    { value: 12, label: '12'},
    { value: 24, label: '24'},
    { value: 48, label: '48'},
];


const ordenName = [
    { value: 'asc', label: 'A-Z'},
    { value: 'desc', label: 'Z-A'},
];


const ordenPrice = [
    { value: 'asc', label: 'Menor A Mayor'},
    { value: 'desc', label: 'Mayor A Menor'},
];


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        margin: theme.spacing(1),
    },
    textField: {
        flexBasis: 800,
    },
}));

const InputAdornments = (props) => {
    const { itemsPerPage,results,sortByNameResult,sortByPriceResult } = props
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <TextField
                select
                label="Productos por pagina"
                className={clsx(classes.margin, classes.textField)}
                value={itemsPerPage}
                onChange={(prop)=>props.setItemsPerPage(prop.target.value)} >
                {ranges.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
            <TextField
                select
                label="Ordenamiento por nombre"
                className={clsx(classes.margin, classes.textField)}
                value={sortByNameResult}
                onChange={(prop)=>props.sortByName(prop.target.value)} >
                {ordenName.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
            <TextField
                select
                label="Ordenamiento por precio"
                className={clsx(classes.margin, classes.textField)}
                value={sortByPriceResult}
                onChange={(prop)=>props.sortByPrice(prop.target.value)} >
                {ordenPrice.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
            <span className={clsx(classes.margin, classes.textField)}>Total: {results.length}</span>
        </div>
    );
}

const mapStateToProps = state => ({
    itemsPerPage: state.itemsPerPage,
    results : state.results,
    sortByNameResult : state.sortByNameResult,
    sortByPriceResult : state.sortByPriceResult
})


export default connect(mapStateToProps, { sortByName,setItemsPerPage,sortByPrice })(InputAdornments);




