import React, { Component } from 'react'
import { connect } from 'react-redux'
import { obtenerCarrito,borrarItem} from '../actions/itemsActions'
import {
    Grid,
    Card,
    CardActionArea,
    CardActions,
    CardMedia,
    Button,
    Paper,
    Typography,
    CardContent,
    withStyles
} from '@material-ui/core';
import {Link} from 'react-router-dom'
import Styles from '../styles'
class Home extends Component {
    removeFavorite = id => event => this.props.borrarItem(id)
    render() {
        let {classes } = this.props
        let items = this.props.items ? this.props.items : []
        return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Typography variant="h5" component="h3">
                                <Link to="/" style={{textDecoration: 'none',color:'black'}}>
                                    {`< regresar`}
                            </Link>
                            </Typography>
                        </Paper>
                    </Grid>
                    {
                        items.map((e)=>{
                            return (
                                <Grid item xs={3}>
                                    <Card className={classes.card}>
                                        <CardActionArea>
                                            <CardMedia
                                                component="img"
                                                image={e.url} alt={e.id}
                                            />
                                             <CardContent>
                                            <Typography gutterBottom variant="body2" >
                                                {e.title}
                                            </Typography>
                                            <Typography gutterBottom variant="body2" >
                                                {`$ ${ e.price ? e.price: ''}`}
                                            </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button size="small" color="primary" onClick = {this.removeFavorite(e.id)}>
                                                Quitar de mi carrito
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid> 
                            )
                        })
                    }
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    results: state.results,
    items: state.items,
    searchFavorite : state.searchFavorite
})

export default connect(mapStateToProps, { obtenerCarrito,borrarItem })(withStyles(Styles)(Home));

