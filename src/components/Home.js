import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { connect } from 'react-redux'
/**
 * 
 */
import { Paper, Typography, Grid, withStyles } from '@material-ui/core';
import Styles from '../styles'
/**
 * 
 */
import { nuevosResults, getSearch } from '../actions/itemsActions'
/**
 * 
 */
import GridList from './Gridresults'
import Filters from './Filters'
import Badge from './Badge'


import store from '../store'


store.subscribe(()=>{
    localStorage.setItem('state',JSON.stringify(store.getState()))
})

class Home extends Component {

    componentDidMount = () => this.props.getSearch({
        items: this.props.items,
        totalPages: this.props.totalPages,
        activePage : this.props.activePage,
        itemsPerPage : this.props.itemsPerPage,
        sortByNameResult : this.props.sortByNameResult,
        sortByPriceResult : this.props.sortByPriceResult
    })

    render() {
        let { classes } = this.props
        return (
            <div className={classes.paper}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper} elevation={0}>
                            <Typography variant="h5" component="h3">
                                <Link to="/items" style={{ textDecoration: 'none', color: 'black' }}>
                                    {`Ir a mi carrito`}
                                </Link>
                                <Badge></Badge>
                            </Typography>
                        </Paper>
                    </Grid>
                    <Grid item lg={2} md={4} sm={12}>
                        <Paper className={classes.paper} elevation={0}>
                            <Filters></Filters>
                        </Paper>
                    </Grid>
                    <Grid item lg={10} md={8} sm={12}>
                        <GridList></GridList>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    results: state.results,
    items: state.items,
    totalPages : state.totalPages,
    activePage : state.activePage,
    itemsPerPage : state.itemsPerPage,
    sortByNameResult : state.sortByNameResult,
    sortByPriceResult : state.sortByPriceResult
})

export default connect(mapStateToProps, { nuevosResults, getSearch })(withStyles(Styles)(Home));

